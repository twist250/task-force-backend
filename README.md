# Awesomity Backend Challenge

Create a To-Do API. The To-Do API should allow the following operations on a todo item:

Create, Update, Read, Delete

### 🏗Object/Modal Structure

A todo item is made of:

- Title
- Description
- Priority(LOW, MEDIUM, HIGH)
- CreateDate(date of creation of the todo item)
- ModifiedDate(date of modification of the todo item)

### 🔖Requirements

- Someone can create / update a todo item by sending:
    - title
    - description
    - priority

    The date of creation should be time stamped automatically when created

- Someone can read one or many todo items
- Someone can delete a todo item

### 👷🏽‍♀️Best Practices

- Document your methods. Tip: [JSDoc](https://jsdoc.app/), [Javadoc](https://docs.oracle.com/javase/7/docs/technotes/tools/windows/javadoc.html)
- Write tests with a minimum of 60% coverage
- Document your API  ([Swagger](https://swagger.io/))
- Validate your request bodies
- Properly log your application
- Have a Dockerized environment(Dockerfile, docker-compose.yml)
- Write a good README file with the steps to Install & run your application (steps for docker included)

### ✨Bonus

Adding an authentication